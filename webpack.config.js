const path = require('path');
const express = require('express')

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/'
  },
  module: {
     rules: [
      {
         test: /\.scss$/,
         use:[{
           loader: 'style-loader',
         },
         {
           loader: 'css-loader',
         },
         {
           loader: 'postcss-loader',
           options:{
             plugins: function(){
               return [
                 require('precss'),
                 require('autoprefixer')
               ];
             }
           }
         },
         {
           loader: 'sass-loader'
         }]
      },
      // {
      //   test: /\.(png|jpe?g|gif)$/i,
      //   use: [
      //     {
      //       loader: 'url-loader',
      //       options: {
      //         limit: 0
      //       }
      //     }
      //   ]
      // },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {              
              emitFile: false,
              name: '[path][name].[ext]',
            }
          }]
      }
     ]
    },
    devServer: {
      contentBase: path.join(__dirname, 'dist'),
      before: (app, server) => {
        app.use('/assets/', express.static(path.join(__dirname, 'assets')))
      }
    }
};
